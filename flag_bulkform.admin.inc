<?php

/**
 * @file
 * Administration page callbacks for the flag_bulkform module
 */

/**
 * Defines the flag_bulkform administrative settings module
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function flag_bulkform_admin_settings() {
  global $user;
  global $base_root;

  // Get an array of node types in the form: array('story' => 'Story', 'page' => 'Page')
  $node_type_options = node_get_types('names');

  $form['flag_bulkform_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show flag-setting options for these node types'),
    '#options' => $node_type_options,
    '#default_value' => variable_get('flag_bulkform_node_types', array('page')),
    '#description' => t('Users will be able to manage flags via FlagForm
      on these node types. However, the node type settings from the
      flag\'s edit page take precedence over the settings above.')
  );

  $form['flag_bulkform_unflagged_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('How many un-flagged items should be inlcuded in the list?'),
    '#default_value' => variable_get('flag_bulkform_unflagged_amount', array(NULL)),
    '#description' => t('Leave blank to show all nodes. Use a positive 
      number to show only a certain number of unflagged nodes (in
      addition to all of the already-flagged nodes).'),
  );

  $flag_options = array();
  $all_flags = flag_get_flags();
  foreach ($all_flags as $flag) {
    if (module_exists('singleflag')) {
      if (!singleflag_is_singleflag($flag->fid)) {
        $flag_options[$flag->fid] = $flag->title;
      }
    }
    else {
      $flag_options[$flag->fid] = $flag->title;
    }
  }

  $form['flag_bulkform_flag_types'] = array(
    '#type' => 'radios',
    '#title' => t('Show flag-setting options for this flag type'),
    '#options' => $flag_options,
    '#default_value' => variable_get('flag_bulkform_flag_types', key($flag_options)),
    '#description' => t('Which flag type do you want to show on the FlagForm page?
      This will eventually be checkboxes and the frontend will let users manage
      more than one flag type through the frontend FlagForm interface. If you 
      have the SingleForm module enabled, SingleFlag flags will not show up here.')
  );

  $form['flag_bulkform_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => variable_get('flag_bulkform_page_title', ''),
    '#description' => t('This will be the page title and the name of the tab for 
      the FlagForm page. You must clear your cache after changing this field.'),
    '#size' => 48,
  );

  $form['flag_bulkform_page_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Page path'),
    '#default_value' => variable_get('flag_bulkform_page_path', 'my-flags'),
    '#description' => t('Where do you want to show the FlagForm page? 
      You must clear your cache after changing this field.'),
    '#field_prefix' => t('!url/user/!uid/', array('!url' => $base_root, '!uid' => $user->uid)),
    '#size' => 25,
  );

  return system_settings_form($form);
} 

/**
 * Validate the admin settings form
 */
function flag_bulkform_admin_settings_validate($form, &$form_state){
  global $user;
  $url = '/user/' . $user->uid . '/' . $form_state['values']['flag_bulkform_page_path'];
  if(!valid_url($url)) {
    form_set_error('flag_bulkform_page_path', t('You\'re path is not valid'));
  }

  $amount = $form_state['values']['flag_bulkform_unflagged_amount'];
  if (!preg_match('/^\d+$/', $amount) && $amount != '') {
    form_set_error('flag_bulkform_unflagged_amount', t('You can only use a 
      positive number or the word \'all\'.'));
  }
}
